﻿using NailsApplication.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace NailsApplication.UserControlCustom
{
    /// <summary>
    /// Interaction logic for MenuItemUC.xaml
    /// </summary>
    public partial class MenuItemUC : UserControl
    {
        public EventHandler ClickItem; //Bắt sự kiện click từ form khác
        
        public bool mSubcribie { get; set; }

        public MenuItemUC(IMenuUC itemMenu)
        {
            InitializeComponent();
            mSubcribie = false;
            ExpanderMenu.Visibility = itemMenu.SubItemUCs == null ? Visibility.Collapsed : Visibility.Visible;
            //ListViewItemMenu.Visibility = itemMenu.SubItemUCs == null ? Visibility.Visible : Visibility.Collapsed;
         
            this.DataContext = itemMenu;
        }

        private void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //data: pass dữ liệu qua parent
            ClickItem(lv_item.SelectedIndex, e); 
        }
    }
}
