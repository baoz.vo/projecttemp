﻿using MaterialDesignThemes.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace NailsApplication.Models
{
    public class IMenuUC
    {
        public PackIconKind Icon { get; set; }
        public List<SubItemUC> SubItemUCs { get; set; }
        public UserControl Screen { get; private set; }
        public string Header { get; set; }

        public IMenuUC(string header, List<SubItemUC> subItemUCs, PackIconKind icon)
        {
            Header = header;
            SubItemUCs = subItemUCs;
            Icon = icon;
        }

        public IMenuUC(string header, UserControl screen, PackIconKind icon)
        {
            Header = header;
            Screen = screen;
            Icon = icon;
        }
    }
}
